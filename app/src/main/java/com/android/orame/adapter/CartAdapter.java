package com.android.orame.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.orame.R;
import com.android.orame.fragment.CartFragment;
import com.android.orame.interfaces.OnRecyclerViewItemClickListener;
import com.android.orame.model.CardModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    List<CardModel> list;
    LayoutInflater inflater;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;

    public CartAdapter(Context ctx, List<CardModel> list, OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.inflater = LayoutInflater.from(ctx);
        this.list = list;
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_cart, parent, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name_cart.setText(list.get(position).getName());
        holder.tv_price_cart.setText(list.get(position).getPrice());
        holder.tv_qty_cart.setText(list.get(position).getqty().toString());

        Glide.with(holder.itemView.getContext())
                .load(list.get(position).getImage())
                .centerCrop()
                .placeholder(R.drawable.background_white)
                .into(holder.iv_cart);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyclerViewItemClickListener.onItemClick(list.get(position), position);
            }
        });

        if (list.get(position).isCheck()) {
            holder.cb_cart.setChecked(true);
        } else {
            holder.cb_cart.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name_cart, tv_price_cart, tv_qty_cart;
        ImageView iv_cart;
        CheckBox cb_cart;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name_cart = itemView.findViewById(R.id.tv_name_cart);
            tv_price_cart = itemView.findViewById(R.id.tv_price_cart);
            tv_qty_cart = itemView.findViewById(R.id.tv_qty_cart);
            iv_cart = itemView.findViewById(R.id.iv_cart);
            cb_cart = itemView.findViewById(R.id.cb_cart);
        }
    }

}


