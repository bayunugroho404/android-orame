package com.android.orame.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.orame.R;
import com.android.orame.interfaces.OnRVHistory;
import com.android.orame.model.CardModel;
import com.android.orame.model.HistoryModel;
import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class HistoryAdapter extends RecyclerView.Adapter<com.android.orame.adapter.HistoryAdapter.ViewHolder> {

    List<HistoryModel> list;
    LayoutInflater inflater;
    private com.android.orame.interfaces.OnRVHistory onRVHistory;

    public HistoryAdapter(Context ctx, List<HistoryModel> list, OnRVHistory OnRVHistory) {
        this.inflater = LayoutInflater.from(ctx);
        this.list = list;
        this.onRVHistory = OnRVHistory;
    }


    @NonNull
    @Override
    public com.android.orame.adapter.HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_history, parent, false);

        return new com.android.orame.adapter.HistoryAdapter.ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull com.android.orame.adapter.HistoryAdapter.ViewHolder holder, int position) {
        holder.tv_name_history.setText(list.get(position).getName());
        holder.tv_price_history.setText(formatRupiah(Double.parseDouble(list.get(position).getPrice())));
        holder.tv_status_history.setText(list.get(position).getStatus());
        holder.tv_tgl_history.setText(list.get(position).getTgl());
        holder.payment_type.setText(formatString(list.get(position).getPayment_type()));
        holder.tv_ongkir_history.setText(formatRupiah(Double.parseDouble(list.get(position).getOngkir())));
        holder.tv_total_history.setText(formatRupiah(Double.parseDouble(list.get(position).getPrice()) + Double.parseDouble(list.get(position).getOngkir())));
        holder.tv_invoice.setText(list.get(position).getInvoice());


        if(list.get(position).getStatus().equals("Success")){
            holder.tv_status_history.setBackgroundColor(R.color.teal_200);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRVHistory.onItemClickHistory(list.get(position), position);
            }
        });

    }

    private String formatString(String payment_type) {
        if(payment_type.equals("tf")){
            return "Transfer";
        }else{
            return "Cash On Delivery";
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name_history, tv_invoice,tv_total_history,tv_ongkir_history,tv_price_history, tv_status_history,tv_tgl_history,payment_type;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name_history = itemView.findViewById(R.id.tv_name_history);
            tv_price_history = itemView.findViewById(R.id.tv_price_history);
            tv_status_history = itemView.findViewById(R.id.tv_status_history);
            tv_tgl_history = itemView.findViewById(R.id.tv_tgl_history);
            payment_type = itemView.findViewById(R.id.payment_type);
            tv_ongkir_history = itemView.findViewById(R.id.tv_ongkir_history);
            tv_total_history = itemView.findViewById(R.id.tv_total_history);
            tv_invoice = itemView.findViewById(R.id.tv_invoice);
        }
    }
    private String formatRupiah(Double number){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);
    }
}



