package com.android.orame.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.orame.R;
import com.android.orame.activity.DetailActivity;
import com.android.orame.model.ModelProduct;
import com.android.orame.model.ModelProductByCategory;
import com.bumptech.glide.Glide;

import java.util.List;

public class ProductByCategoryAdapter extends RecyclerView.Adapter<ProductByCategoryAdapter.ViewHolder> {

    List<ModelProductByCategory> list;
    LayoutInflater inflater;

    public ProductByCategoryAdapter(Context ctx, List<ModelProductByCategory> list){
        this.list = list;
        this.inflater = LayoutInflater.from(ctx);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_product,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_name.setText(list.get(position).getName());

        Glide.with(holder.itemView.getContext())
                .load(list.get(position).getImage())
                .centerCrop()
                .placeholder(R.drawable.background_white)
                .into(holder.iv_product);

        holder.card_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(holder.itemView.getContext(), DetailActivity.class);
                i.putExtra("name",list.get(position).getName() );
                i.putExtra("image",list.get(position).getImage() );
                i.putExtra("price",list.get(position).getPrice() );
                i.putExtra("stok",list.get(position).getStok() );
                holder.itemView.getContext().startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_price;
        ImageView iv_product;
        CardView card_product;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            card_product = itemView.findViewById(R.id.card_product);
            tv_price = itemView.findViewById(R.id.tv_price);
            iv_product = itemView.findViewById(R.id.iv_product);
        }
    }
}
