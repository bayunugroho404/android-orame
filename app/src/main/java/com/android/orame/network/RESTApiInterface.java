package com.android.orame.network;

import com.android.orame.network.response.add_cart.ResponseAddCart;
import com.android.orame.network.response.login.ResponseLogin;
import com.android.orame.network.response.order.ResponseOrder;
import com.android.orame.network.response.product.ResponseProduct;
import com.android.orame.network.response.product_category.ResponseProductCategory;
import com.android.orame.network.response.response_get_cart.ResponseGetCart;
import com.android.orame.network.response.response_history.ResponseHistory;
import com.android.orame.network.response.status.ResponseStatus;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RESTApiInterface {

    @FormUrlEncoded
    @POST("/api/login")
    Call<ResponseLogin> postLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("/api/register")
    Call<ResponseLogin> postRegister(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation
    );


    @GET("/api/product")
    Call<List<ResponseProduct>> getProduct();


    @GET("/api/history/{id}")
    Call<List<ResponseHistory>> getHistory(
        @Path("id")String id
    );
    @GET("/api/transaction-now/{id}")
    Call<List<ResponseHistory>> getHistoryBerlangsung(
        @Path("id")String id
    );

    @GET("/api/product/{cat}")
    Call<List<ResponseProductCategory>> getProductCategory(
            @Path("cat") String cat);


    @FormUrlEncoded
    @POST("/api/get_cart")
    Call<List<ResponseGetCart>> getCart(
            @Field("users_id") String users_id
    );


    @FormUrlEncoded
    @POST("/api/cart")
    Call<ResponseAddCart> addCart(
            @Field("kuantitas") String kuantitas,
            @Field("products_id") String products_id,
            @Field("users_id") String users_id
    );

    @Multipart
    @POST("/api/order")
    Call<ResponseOrder> postOrder( @Part MultipartBody.Part file,
                                   @Part("name") RequestBody name,
                                   @Part("product_id") RequestBody product_id,
                                   @Part("number_phone") RequestBody number_phone,
                                   @Part("qty") RequestBody qty,
                                   @Part("price") RequestBody price,
                                   @Part("note") RequestBody note,
                                   @Part("payment_type") RequestBody payment_type,
                                   @Part("address") RequestBody address,
                                   @Part("users_id") RequestBody user_id);

    @Multipart
    @POST("/api/tf")
    Call<ResponseStatus> updateTransfer(@Part MultipartBody.Part file,
                                   @Part("id") RequestBody id);


    @FormUrlEncoded
    @POST("/api/order")
    Call<ResponseOrder> postOrderWithoutImage(
            @Field("name") String name,
            @Field("product_id") String product_id,
            @Field("number_phone") String number_phone,
            @Field("qty") String qty,
            @Field("price") String price,
            @Field("note") String note,
            @Field("payment_type") String payment_type,
            @Field("address") String address,
            @Field("users_id") String user,
            @Field("ongkir") String ongkir
    );
}
