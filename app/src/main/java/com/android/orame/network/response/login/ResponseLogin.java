package com.android.orame.network.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

	@SerializedName("status_code")
	@Expose
	private Integer statusCode;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("token")
	@Expose
	private String token;
	@SerializedName("datum")
	@Expose
	private Datum datum;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Datum getDatum() {
		return datum;
	}

	public void setDatum(Datum datum) {
		this.datum = datum;
	}


}
