package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.add_cart.ResponseAddCart;
import com.android.orame.network.response.login.ResponseLogin;
import com.bumptech.glide.Glide;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    ImageView iv_detail;
    TextView tv_detail_name, tv_stok, tv_detail_price, tv_detail_ket;
    private RESTApiInterface service;
    Button btn_add_cart, btn_chat_seller;
    EditText edt_jumlh;
    String userId;
    Integer stock = 0;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        service = RESTApiClient.createBaseService(this, RESTApiInterface.class);
        userId = Preferences.getUserId(DetailActivity.this);

        iv_detail = (ImageView) findViewById(R.id.iv_detail);
        edt_jumlh = (EditText) findViewById(R.id.edt_jumlh);
        btn_add_cart = (Button) findViewById(R.id.btn_add_cart);
        btn_chat_seller = (Button) findViewById(R.id.btn_chat_seller);
        tv_detail_name = (TextView) findViewById(R.id.tv_detail_name);
        tv_detail_price = (TextView) findViewById(R.id.tv_detail_price);
        tv_stok = (TextView) findViewById(R.id.tv_stok);
        tv_detail_ket = (TextView) findViewById(R.id.tv_detail_ket);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            String name = (String) bundle.get("name");
            String id = (String) bundle.get("id");
            String price = (String) bundle.get("price");
            String image = (String) bundle.get("image");
            String ket = (String) bundle.get("ket");
            stock = (Integer) bundle.get("stok");

            tv_detail_name.setText(name);
            tv_detail_price.setText(price);
            tv_stok.setText("(" + stock.toString() + ")");
            tv_detail_ket.setText("Keterangan : " + ket);
            Glide.with(DetailActivity.this)
                .load(image)
                .centerCrop()
                .placeholder(R.drawable.background_white)
                .into(iv_detail);
            btn_chat_seller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    boolean isInstalled = appInstalledOrNot("com.whatsapp");

//                    if (isInstalled) {
                    Intent moveWa = new Intent(Intent.ACTION_VIEW);
                    moveWa.setData(Uri.parse("http://api.whatsapp.com/send?phone=+628158715963&text=Apakah " + name + " Barang ini ada?"));
                    startActivity(moveWa);
//                    } else {
//                        Intent moveGooglePlay = new Intent(Intent.ACTION_VIEW);
//                        moveGooglePlay.setData(Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp"));
//                        startActivity(moveGooglePlay);
//                    }
                }
            });
            btn_add_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (edt_jumlh.getText().toString().isEmpty()) {
                        Toast.makeText(DetailActivity.this, "kuantitas tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    } else if (Integer.parseInt(edt_jumlh.getText().toString()) > stock) {
                        Toast.makeText(DetailActivity.this, "kuantitas terlalu banyak", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DetailActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();

                                                Call<ResponseAddCart> call = service.addCart(edt_jumlh.getText().toString(), id, userId);
                        call.enqueue(new Callback<ResponseAddCart>() {
                            @Override
                            public void onResponse(Call<ResponseAddCart> call, Response<ResponseAddCart> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(DetailActivity.this, "Berhasil tambah ke keranjang", Toast.LENGTH_SHORT).show();
                                    edt_jumlh.setText("");
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseAddCart> call, Throwable t) {

                            }
                        });
                    }
                }
            });
        }


    }

    private boolean appInstalledOrNot(String url) {
        PackageManager packageManager = getPackageManager();
        boolean app_installed;
        try {
            packageManager.getPackageInfo(url, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
