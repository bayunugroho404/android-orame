package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.orame.MainActivity;
import com.android.orame.R;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.status.ResponseStatus;
import com.bumptech.glide.Glide;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.orame.activity.OrderActivity.verifyStoragePermissions;

public class UpdateTransferActivity extends AppCompatActivity {

    String mediaPath, mediaPath1;
    String id = "";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private RESTApiInterface service;
    ImageView ivImage,ivImageDefault;
    int SELECT_PICTURE = 200;
    Button btn_choose, btn_upload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_transfer);
        btn_upload = (Button) findViewById(R.id.btn_uploads);
        btn_choose = (Button) findViewById(R.id.btn_choose);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        ivImageDefault = (ImageView) findViewById(R.id.ivImageDefault);
        service = RESTApiClient.createBaseService(this, RESTApiInterface.class);
        verifyStoragePermissions(UpdateTransferActivity.this);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            id = (String) bundle.get("id");
            String image = (String) bundle.get("image");
            Log.e("ss ",image);
            Glide.with(UpdateTransferActivity.this)
                .load(image)
                .centerCrop()
                .placeholder(R.drawable.background_white)
                .into(ivImageDefault);

            btn_choose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageChooser();
                }
            });

            btn_upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file = new File(mediaPath);
                    RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                    RequestBody idValue = RequestBody.create(MediaType.parse("text/plain"), id);
                    Call<ResponseStatus> call = service.updateTransfer(fileToUpload,idValue);
                    call.enqueue(new Callback<ResponseStatus>() {
                        @Override
                        public void onResponse(Call<ResponseStatus> call, Response<ResponseStatus> response) {
                            if(response.body().getStatus().equals("OK")){
                                Toast.makeText(UpdateTransferActivity.this, "Berhasil update bukti transfer", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(UpdateTransferActivity.this, MainActivity.class);
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseStatus> call, Throwable t) {

                        }
                    });
                }
            });
        }
    }

    private void imageChooser() {
        verifyStoragePermissions(UpdateTransferActivity.this);
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                Log.e("dsdasdsa ", mediaPath);
                ivImage.setVisibility(View.VISIBLE);
                btn_upload.setVisibility(View.VISIBLE);
                ivImage.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();

            } // When an Video is picked
            else if (requestCode == 1 && resultCode == RESULT_OK && null != data) {

                // Get the Video from data
                Uri selectedVideo = data.getData();
                String[] filePathColumn = {MediaStore.Video.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                mediaPath1 = cursor.getString(columnIndex);
                // Set the Video Thumb in ImageView Previewing the Media
                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

}
