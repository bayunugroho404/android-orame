package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.android.orame.R;
import com.android.orame.adapter.ProductAdapter;
import com.android.orame.adapter.ProductByCategoryAdapter;
import com.android.orame.model.ModelProduct;
import com.android.orame.model.ModelProductByCategory;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.product.ResponseProduct;
import com.android.orame.network.response.product_category.ResponseProductCategory;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductByCategoryActivity extends AppCompatActivity {
    RecyclerView rcProductCategory;
    ProductByCategoryAdapter productByCategoryAdapter;
    private RESTApiInterface service;
    List<ModelProductByCategory> listProduct = new ArrayList<ModelProductByCategory>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_by_category);
        service = RESTApiClient.createBaseService(ProductByCategoryActivity.this, RESTApiInterface.class);
        rcProductCategory = (RecyclerView) findViewById(R.id.rcProductCategory);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            String title = (String) bundle.get("title");
            setTitle(title);
            getData(title);
        }


    }

    private void getData(String cat) {
        Call<List<ResponseProductCategory>> call = service.getProductCategory(cat);
        call.enqueue(new Callback<List<ResponseProductCategory>>() {
            @Override
            public void onResponse(Call<List<ResponseProductCategory>> call, Response<List<ResponseProductCategory>> response) {
                listProduct.clear();
                for (int i = 0; i < response.body().size(); i++) {
                    ModelProductByCategory modelProduct = new ModelProductByCategory(
                            response.body().get(i).getName(), response.body().get(i).getHarga().toString(), response.body().get(i).getImage(), response.body().get(i).getId().toString(), response.body().get(i).getKeterangan(),response.body().get(i).getJumlah());
                    listProduct.add(modelProduct);

                    productByCategoryAdapter = new ProductByCategoryAdapter(ProductByCategoryActivity.this, listProduct);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(ProductByCategoryActivity.this, 2, GridLayoutManager.VERTICAL, false);
                    rcProductCategory.setLayoutManager(gridLayoutManager);
                    rcProductCategory.setAdapter(productByCategoryAdapter);

                }
            }

            @Override
            public void onFailure(Call<List<ResponseProductCategory>> call, Throwable t) {

            }
        });
    }
}
