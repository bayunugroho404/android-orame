package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.orame.MainActivity;
import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.login.ResponseLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.orame.Preferences.KEY_STATUS_SEDANG_LOGIN;
import static com.android.orame.Preferences.TOKEN;
import static com.android.orame.Preferences.USER_ID;
import static com.android.orame.Preferences.NAME;
import static com.android.orame.Preferences.EMAIL;
import static com.android.orame.Preferences.getSharedPreference;

public class LoginActivity extends AppCompatActivity {
    TextView tv_register;
    EditText edt_email, edt_password;
    Button btn_sign_in;
    private RESTApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException ignored) {
        }

        checkLogin();
        tv_register = (TextView) findViewById(R.id.tv_register);
        btn_sign_in = (Button) findViewById(R.id.btn_sign_in);
        edt_email = (EditText) findViewById(R.id.edt_email_login);
        edt_password = (EditText) findViewById(R.id.edt_password_login);
        service = RESTApiClient.createBaseService(this, RESTApiInterface.class);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<ResponseLogin> call = service.postLogin(edt_email.getText().toString(), edt_password.getText().toString());
                call.enqueue(new Callback<ResponseLogin>() {
                    @Override
                    public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                        if (response.isSuccessful()) {
                            SharedPreferences.Editor editor = getSharedPreference(LoginActivity.this).edit();
                            editor.putBoolean(KEY_STATUS_SEDANG_LOGIN, true);
                            editor.putString(TOKEN, response.body().getToken());
                            editor.putString(USER_ID, response.body().getDatum().getId().toString());
                            editor.putString(EMAIL, response.body().getDatum().getEmail().toString());
                            editor.putString(NAME, response.body().getDatum().getName().toString());
                            editor.apply();

                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(LoginActivity.this, "akun tidak diketahui", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseLogin> call, Throwable t) {
                        Log.e("sass2 ", t.getMessage());
                    }
                });
            }
        });


        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, ResigterActivity.class);
                startActivity(i);
            }
        });

    }

    private void checkLogin() {
        boolean status= Preferences.getLoggedInUser(LoginActivity.this);
        if(status){
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }
    }
}
