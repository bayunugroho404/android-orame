package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.adapter.CartAdapter;
import com.android.orame.adapter.HistoryAdapter;
import com.android.orame.fragment.CartFragment;
import com.android.orame.interfaces.OnRVHistory;
import com.android.orame.interfaces.OnRecyclerViewItemClickListener;
import com.android.orame.model.CardModel;
import com.android.orame.model.HistoryModel;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.response_get_cart.ResponseGetCart;
import com.android.orame.network.response.response_history.ResponseHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity implements OnRVHistory {

    RecyclerView recyclerViewHistory;
    HistoryAdapter historyAdapter;
    List<HistoryModel> listHistory = new ArrayList<HistoryModel>();
    private RESTApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setTitle("History Transaksi");
        recyclerViewHistory = (RecyclerView) findViewById(R.id.recyclerViewHistory);
        service = RESTApiClient.createBaseService(HistoryActivity.this, RESTApiInterface.class);
        getHistory();
    }

    private void getHistory() {
        String user_id = Preferences.getUserId(HistoryActivity.this);
        Call<List<ResponseHistory>> call = service.getHistory(user_id);
        call.enqueue(new Callback<List<ResponseHistory>>() {
            @Override
            public void onResponse(Call<List<ResponseHistory>> call, Response<List<ResponseHistory>> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        HistoryModel historyModel = new HistoryModel(
                            response.body().get(i).getId().toString(), response.body().get(i).getTransactionStatus(),
                            response.body().get(i).getName(), response.body().get(i).getCreatedAt(),
                            response.body().get(i).getPrice().toString(),response.body().get(i).getPaymentType(),
                            response.body().get(i).getOngkir().toString(),response.body().get(i).getInvoiceId(),
                            response.body().get(i).getImage()
                            );
                        listHistory.add(historyModel);
                    }
                    if (listHistory.size() > 0) {
                        historyAdapter = new HistoryAdapter(HistoryActivity.this,
                            listHistory, HistoryActivity.this);

                        GridLayoutManager gridLayoutManager = new GridLayoutManager(HistoryActivity.this,
                            1, GridLayoutManager.VERTICAL, false);
                        recyclerViewHistory.setLayoutManager(gridLayoutManager);
                        recyclerViewHistory.setAdapter(historyAdapter);
                    }else{
                        Toast.makeText(HistoryActivity.this, "tidak ada history transaksi", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<ResponseHistory>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onItemClickHistory(HistoryModel model, int position) {

    }
}
