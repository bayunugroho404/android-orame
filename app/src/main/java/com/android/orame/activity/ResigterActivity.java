package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.orame.R;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.login.ResponseLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResigterActivity extends AppCompatActivity {
    TextView tv_login;
    EditText edt_name;
    EditText edt_email;
    EditText edt_password;
    EditText edt_c_password;
    Button btn_sign_up;
    private RESTApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resigter);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException ignored) {
        }

        tv_login = (TextView) findViewById(R.id.tv_login);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_c_password = (EditText) findViewById(R.id.edt_c_password);
        btn_sign_up = (Button) findViewById(R.id.btn_sign_up);
        service = RESTApiClient.createBaseService(this, RESTApiInterface.class);

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<ResponseLogin> call = service.postRegister(edt_name.getText().toString(),
                        edt_email.getText().toString(), edt_password.getText().toString(), edt_c_password.getText().toString());
                if(edt_password.getText().toString().equals(edt_c_password.getText().toString())){
                    call.enqueue(new Callback<ResponseLogin>() {
                        @Override
                        public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(ResigterActivity.this, "Berhasil Register", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(ResigterActivity.this, LoginActivity.class);
                                startActivity(i);
                            }else{
                                Toast.makeText(ResigterActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseLogin> call, Throwable t) {

                        }
                    });
                }else{
                    Toast.makeText(ResigterActivity.this, "Password tidak sama", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ResigterActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }
}
