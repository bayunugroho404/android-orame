package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.adapter.HistoryAdapter;
import com.android.orame.adapter.HistoryBAdapter;
import com.android.orame.interfaces.OnRVHistory;
import com.android.orame.interfaces.TransferListener;
import com.android.orame.model.HistoryBModel;
import com.android.orame.model.HistoryModel;
import com.android.orame.model.HistoryModel;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.response_get_cart.ResponseGetCart;
import com.android.orame.network.response.response_history.ResponseHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryBActivity extends AppCompatActivity implements TransferListener {

    RecyclerView recyclerViewHistory;
    HistoryBAdapter historyAdapter;
    List<HistoryBModel> listHistory = new ArrayList<HistoryBModel>();
    private RESTApiInterface service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_b);
        setTitle("Transaksi Berlangsung");
        recyclerViewHistory = (RecyclerView) findViewById(R.id.recyclerViewHistoryB);
        service = RESTApiClient.createBaseService(HistoryBActivity.this, RESTApiInterface.class);
        getHistory();
    }
    private void getHistory() {
        String user_id = Preferences.getUserId(HistoryBActivity.this);
        Call<List<ResponseHistory>> call = service.getHistoryBerlangsung(user_id);
        call.enqueue(new Callback<List<ResponseHistory>>() {
            @Override
            public void onResponse(Call<List<ResponseHistory>> call, Response<List<ResponseHistory>> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        HistoryBModel historyModel = new HistoryBModel(
                            response.body().get(i).getId().toString(), response.body().get(i).getTransactionStatus(),
                            response.body().get(i).getName(), response.body().get(i).getCreatedAt(),
                            response.body().get(i).getPrice().toString(),response.body().get(i).getPaymentType(),
                            response.body().get(i).getOngkir().toString(),response.body().get(i).getInvoiceId(),
                            response.body().get(i).getImage()
                        );
                        listHistory.add(historyModel);
                    }
                    if (listHistory.size() > 0) {
                        historyAdapter = new HistoryBAdapter(HistoryBActivity.this,
                            listHistory,HistoryBActivity.this);

                        GridLayoutManager gridLayoutManager = new GridLayoutManager(HistoryBActivity.this,
                            1, GridLayoutManager.VERTICAL, false);
                        recyclerViewHistory.setLayoutManager(gridLayoutManager);
                        recyclerViewHistory.setAdapter(historyAdapter);
                    }else{
                        Toast.makeText(HistoryBActivity.this, "tidak ada history transaksi", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<ResponseHistory>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onItemClick(HistoryBModel model, int position) {
        Intent i = new Intent(HistoryBActivity.this, UpdateTransferActivity.class);
        i.putExtra("id",model.getId() );
        i.putExtra("image",model.getImage() );
        startActivity(i);
    }
}
