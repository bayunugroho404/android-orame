package com.android.orame.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.orame.MainActivity;
import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.login.ResponseLogin;
import com.android.orame.network.response.order.ResponseOrder;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch s_payment;
    String mediaPath, mediaPath1;
    Button btn_upload, btn_order;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ImageView IVPreviewImage;
    int SELECT_PICTURE = 200;
    String total = "";
    String payment_type = "cod";
    private RESTApiInterface service;
    String id_product = "";
    String qty_product = "";
    EditText edt_penerima, edt_ongkir,edt_alamat_order, edt_kontak_order, edt_note_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        s_payment = findViewById(R.id.s_payment);
        btn_upload = (Button) findViewById(R.id.btn_upload);
        btn_order = (Button) findViewById(R.id.btn_order);
        edt_penerima = (EditText) findViewById(R.id.edt_penerima);
        edt_alamat_order = (EditText) findViewById(R.id.edt_alamat_order);
        edt_kontak_order = (EditText) findViewById(R.id.edt_kontak_order);
        edt_ongkir = (EditText) findViewById(R.id.edt_ongkir);
        edt_note_order = (EditText) findViewById(R.id.edt_note_order);
        IVPreviewImage = (ImageView) findViewById(R.id.IVPreviewImage);
        service = RESTApiClient.createBaseService(this, RESTApiInterface.class);

        verifyStoragePermissions(OrderActivity.this);
        String email = Preferences.getEmail(OrderActivity.this);

        edt_penerima.setText(email);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            total = (String) bundle.get("total");
            id_product = (String) bundle.get("id_product");
            qty_product = (String) bundle.get("qty_product");

            setTitle("Rp. " + total);
        }

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageChooser();
            }
        });

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_id = Preferences.getUserId(OrderActivity.this);

//                verifyStoragePermissions(OrderActivity.this);


                Call<ResponseOrder> call = service.postOrderWithoutImage(
                    edt_penerima.getText().toString(), id_product, edt_kontak_order.getText().toString(), qty_product,
                    total,
                    edt_note_order.getText().toString(),
                    payment_type,
                    edt_alamat_order.getText().toString(),
                    user_id,
                    edt_ongkir.getText().toString()
                );
                call.enqueue(new Callback<ResponseOrder>() {
                    @Override
                    public void onResponse(Call<ResponseOrder> call, Response<ResponseOrder> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getMessage().equals("successfull")) {
                                Toast.makeText(OrderActivity.this, "Berhasil Order", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(OrderActivity.this, MainActivity.class);
                                startActivity(i);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseOrder> call, Throwable t) {
                    }
                });
//                if (payment_type.equals("tf")) {
//                    File file = new File(mediaPath);
//
//                    RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
//                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
//                    RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), edt_penerima.getText().toString());
//                    RequestBody product_id = RequestBody.create(MediaType.parse("text/plain"), id_product);
//                    RequestBody number_phone = RequestBody.create(MediaType.parse("text/plain"), edt_kontak_order.getText().toString());
//                    RequestBody qty = RequestBody.create(MediaType.parse("text/plain"), qty_product);
//                    RequestBody price = RequestBody.create(MediaType.parse("text/plain"), total);
//                    RequestBody note = RequestBody.create(MediaType.parse("text/plain"), edt_note_order.getText().toString());
//                    RequestBody address = RequestBody.create(MediaType.parse("text/plain"), edt_alamat_order.getText().toString());
//                    RequestBody user = RequestBody.create(MediaType.parse("text/plain"), user_id);
//                    RequestBody payment_types = RequestBody.create(MediaType.parse("text/plain"), payment_type);
//                    Call<ResponseOrder> call = service.postOrder(
//                            fileToUpload,
//                            filename,
//                            product_id,
//                            number_phone,
//                            qty,
//                            price,
//                            note,
//                            payment_types,
//                            address,user);
//                    call.enqueue(new Callback<ResponseOrder>() {
//                        @Override
//                        public void onResponse(Call<ResponseOrder> call, Response<ResponseOrder> response) {
//                            if (response.isSuccessful()) {
//                                if (response.body().getMessage().equals("successfull")) {
//                                    Toast.makeText(OrderActivity.this, "Berhasil Order", Toast.LENGTH_SHORT).show();
//                                    Intent i = new Intent(OrderActivity.this, MainActivity.class);
//                                    startActivity(i);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseOrder> call, Throwable t) {
//                        }
//                    });
//                } else {
//                    Call<ResponseOrder> call = service.postOrderWithoutImage(
//                            edt_penerima.getText().toString(), id_product, edt_kontak_order.getText().toString(), qty_product,
//                            total,
//                            edt_note_order.getText().toString(),
//                            payment_type,
//                            edt_alamat_order.getText().toString(),
//                            user_id
//                    );
//                    call.enqueue(new Callback<ResponseOrder>() {
//                        @Override
//                        public void onResponse(Call<ResponseOrder> call, Response<ResponseOrder> response) {
//                            if (response.isSuccessful()) {
//                                if (response.body().getMessage().equals("successfull")) {
//                                    Toast.makeText(OrderActivity.this, "Berhasil Order", Toast.LENGTH_SHORT).show();
//                                    Intent i = new Intent(OrderActivity.this, MainActivity.class);
//                                    startActivity(i);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseOrder> call, Throwable t) {
//                        }
//                    });
//                }
            }
        });

        s_payment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btn_upload.setVisibility(View.GONE);
                    IVPreviewImage.setVisibility(View.GONE);
                    payment_type = "tf";
                } else {
                    payment_type = "cod";
                    btn_upload.setVisibility(View.GONE);
                    IVPreviewImage.setVisibility(View.GONE);
                }
            }
        });

    }

    private void imageChooser() {
        verifyStoragePermissions(OrderActivity.this);
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                Log.e("dsdasdsa ", mediaPath);
//                IVPreviewImage.setImageBitmap(BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage)));
                IVPreviewImage.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();

            } // When an Video is picked
            else if (requestCode == 1 && resultCode == RESULT_OK && null != data) {

                // Get the Video from data
                Uri selectedVideo = data.getData();
                String[] filePathColumn = {MediaStore.Video.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                mediaPath1 = cursor.getString(columnIndex);
                // Set the Video Thumb in ImageView Previewing the Media
                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }


//        if (resultCode == RESULT_OK) {
//            if (requestCode == SELECT_PICTURE) {
//                Uri selectedImageUri = data.getData();
//                if (null != selectedImageUri) {
//                    Log.e(" dasdas  ", data.getData().getPath());
//                    IVPreviewImage.setImageURI(selectedImageUri);
//                }
//            }
//        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
