package com.android.orame.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.activity.DetailActivity;
import com.android.orame.activity.LoginActivity;
import com.android.orame.activity.OrderActivity;
import com.android.orame.adapter.CartAdapter;
import com.android.orame.adapter.ProductAdapter;
import com.android.orame.interfaces.OnRecyclerViewItemClickListener;
import com.android.orame.model.CardModel;
import com.android.orame.model.ModelProduct;
import com.android.orame.network.RESTApiClient;
import com.android.orame.network.RESTApiInterface;
import com.android.orame.network.response.product.ResponseProduct;
import com.android.orame.network.response.response_get_cart.ResponseGetCart;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment implements OnRecyclerViewItemClickListener{
    RecyclerView recyclerViewCart;
    OnRecyclerViewItemClickListener listener;
    Button btn_checkout;
    TextView tv_total;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    CartAdapter cartAdapter;
    Integer total= 0;
    List<Integer> listIdProduct = new ArrayList<Integer>();
    List<Integer> listQtyProduct = new ArrayList<Integer>();
    List<CardModel> listCart = new ArrayList<CardModel>();
    private RESTApiInterface service;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public CartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        recyclerViewCart = (RecyclerView) view.findViewById(R.id.recyclerViewCart);
        tv_total = (TextView) view.findViewById(R.id.tv_total);
        btn_checkout = (Button) view.findViewById(R.id.btn_checkout);
        service = RESTApiClient.createBaseService(getContext(), RESTApiInterface.class);
        getCart();

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(), OrderActivity.class);
                i.putExtra("id_product",listIdProduct.toString() );
                i.putExtra("qty_product",listQtyProduct.toString() );
                i.putExtra("total", total.toString() );
                getActivity().startActivity(i);
            }
        });
        return view;
    }

    private void getCart() {
        String user_id = Preferences.getUserId(getActivity());

        Call<List<ResponseGetCart>> call = service.getCart(user_id);
        call.enqueue(new Callback<List<ResponseGetCart>>() {
            @Override
            public void onResponse(Call<List<ResponseGetCart>> call, Response<List<ResponseGetCart>> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        CardModel cardModel = new CardModel(
                                response.body().get(i).getProduct().getName(), response.body().get(i).getProduct().getHarga().toString()
                                , false, response.body().get(i).getProduct().getImage(),
                                response.body().get(i).getKuantitas(),
                                response.body().get(i).getProduct().getHarga() * response.body().get(i).getKuantitas(),response.body().get(i).getProduct().getId());
                        listCart.add(cardModel);
                    }
                    if (listCart.size() > 0) {
                        if (getActivity() != null) {
                            cartAdapter = new CartAdapter(getActivity(), listCart, CartFragment.this);

                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false);
                            recyclerViewCart.setLayoutManager(gridLayoutManager);
                            recyclerViewCart.setAdapter(cartAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ResponseGetCart>> call, Throwable t) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onItemClick(CardModel model, int position) {
        if(!model.isCheck()){
            model.setCheck(true);
            listIdProduct.add(model.getId());
            listQtyProduct.add(model.getqty());
            total += model.getTotal();
            tv_total.setText(total.toString());
            cartAdapter.notifyDataSetChanged();
        }else{
            model.setCheck(false);
            total -= model.getTotal();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                listIdProduct.removeIf(value  -> value .equals(model.getId()));
                listQtyProduct.removeIf(value  -> value .equals(model.getQty()));
            }
            tv_total.setText(total.toString());
            cartAdapter.notifyDataSetChanged();
        }
    }
}
