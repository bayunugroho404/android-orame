package com.android.orame.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.orame.MainActivity;
import com.android.orame.Preferences;
import com.android.orame.R;
import com.android.orame.activity.HistoryActivity;
import com.android.orame.activity.HistoryBActivity;
import com.android.orame.activity.LoginActivity;

import org.w3c.dom.Text;

import static com.android.orame.Preferences.KEY_STATUS_SEDANG_LOGIN;
import static com.android.orame.Preferences.TOKEN;
import static com.android.orame.Preferences.getSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Button btn_logout, btn_chat_admin,btn_history,btn_history_berlangsung;
    TextView tv_email_profile,tv_name_profile;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        btn_logout = (Button) view.findViewById(R.id.btn_logout);
        btn_history = (Button) view.findViewById(R.id.btn_historys);
        btn_history_berlangsung = (Button) view.findViewById(R.id.btn_history_berlangsung);
        btn_chat_admin = (Button) view.findViewById(R.id.btn_chat_admin);

        tv_email_profile = (TextView) view.findViewById(R.id.tv_email_profile);
        tv_name_profile = (TextView) view.findViewById(R.id.tv_name_profile);

        String name = Preferences.getName(getActivity());
        String email = Preferences.getEmail(getActivity());


        tv_name_profile.setText(name);
        tv_email_profile.setText(email);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = getSharedPreference(getContext()).edit();
                editor.putBoolean(KEY_STATUS_SEDANG_LOGIN, false);
                editor.apply();

                startActivity(new
                        Intent(getContext(), LoginActivity.class));
            }
        });
        btn_chat_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveWa = new Intent(Intent.ACTION_VIEW);
                moveWa.setData(Uri.parse("http://api.whatsapp.com/send?phone=+628561326888&text=Apakah Pesanan Saya Sudah di Approve?"));
                startActivity(moveWa);
            }
        });

        btn_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), HistoryActivity.class);
                startActivity(i);
            }
        });
        btn_history_berlangsung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), HistoryBActivity.class);
                startActivity(i);
            }
        });
        return view;
    }
}
