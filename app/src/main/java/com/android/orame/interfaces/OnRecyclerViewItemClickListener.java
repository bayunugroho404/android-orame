package com.android.orame.interfaces;

import com.android.orame.model.CardModel;
import com.android.orame.model.HistoryModel;

public interface OnRecyclerViewItemClickListener {
    void onItemClick(CardModel model,int position);
}
