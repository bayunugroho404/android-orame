package com.android.orame.interfaces;

import com.android.orame.model.HistoryBModel;

public interface TransferListener {
    void onItemClick(HistoryBModel model, int position);
}
