package com.android.orame.interfaces;

import com.android.orame.model.CardModel;
import com.android.orame.model.HistoryModel;

public interface OnRVHistory {
    void onItemClickHistory(HistoryModel model, int position);
}
