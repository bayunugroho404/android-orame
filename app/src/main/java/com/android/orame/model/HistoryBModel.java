package com.android.orame.model;

public class HistoryBModel {
    String id;
    String status;
    String name;
    String tgl;
    String price;
    String payment_type;
    String ongkir;
    String invoice;
    String image;


    public HistoryBModel(String id,String status, String name, String tgl, String price, String payment_type,String ongkir, String invoice, String image) {
        this.id = id;
        this.status = status;
        this.name = name;
        this.tgl = tgl;
        this.price = price;
        this.payment_type = payment_type;
        this.ongkir=  ongkir;
        this.invoice = invoice;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOngkir() {
        return ongkir;
    }

    public void setOngkir(String ongkir) {
        this.ongkir = ongkir;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

