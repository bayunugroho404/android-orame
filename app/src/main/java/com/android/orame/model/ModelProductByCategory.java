package com.android.orame.model;

public class ModelProductByCategory {

    String name;
    String price;
    String image;
    String id;
    String ket;
    Integer stok;


    public ModelProductByCategory(String name, String price, String image,String id,String ket,Integer stok) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.id = id;
        this.ket = ket;
        this.stok = stok;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }
}
