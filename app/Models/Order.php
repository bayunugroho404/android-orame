<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'product_id', 'number_phone', 'qty', 'price', 'note','payment_type','address','image'
    ];

    public function getImageAttribute($name)
    {
        return asset('storage/uploads/' . $name);
    }

}
