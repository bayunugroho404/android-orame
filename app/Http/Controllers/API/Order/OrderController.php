<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{

    public function store(Request $request){

        try{
            if ($request->file('image')) {
                $path =$request->file('image')->store(null, 'uploads');
                $data = Order::create($request->toArray());

                return response()->json([
                    "success" => true,
                    "message" => "successfull",
                    "file" => $path
                ]);

            }else{
             $data = Order::create($request->toArray());
             return response()->json([
                "success" => true,
                "message" => "successfull",
            ]);

         }


     }catch(Exception $error){
        return response()->json([
            'status_code' => 500,
            'message' => 'Failed',
            'error' => $error,
        ]);
    }


}}
